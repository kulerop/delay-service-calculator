﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Sunny.UI;

namespace 延时服务费用发放
{
    public partial class form1 : UIForm
    {
        public form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            for(int i = 0; i < 50; i++)
            {
               string a= i.ToString();
                uiComboBox1.Items.Add(a);
                uiComboBox2.Items.Add(a);                
            }
            for(int b = 1; b <= 12; b++)
            {
                string c = b.ToString();
                uiComboBox3.Items.Add(c);
            }
            uiComboBox4.Items.Add("一年级");
            uiComboBox4.Items.Add("二年级");
            uiComboBox4.Items.Add("三年级");
            uiComboBox4.Items.Add("四年级");
            uiComboBox4.Items.Add("五年级");
            uiComboBox4.Items.Add("六年级");
            for(int g = 1; g <= 20; g++)
            {
                string Class = g.ToString() + "班";
                uiComboBox5.Items.Add(Class);
            }
        }
        // 禁止输入非数字
        private void uiTextBox1_KeyPress(object sender, KeyPressEventArgs e)   
        {
            if (e.KeyChar == 0x20) e.KeyChar = (char)0;  //禁止空格键  
            if ((e.KeyChar == 0x2D) && (((TextBox)sender).Text.Length == 0)) return;   //处理负数  
            if (e.KeyChar > 0x20)
            {
                try
                {
                    double.Parse(((TextBox)sender).Text + e.KeyChar.ToString());
                }
                catch
                {
                    e.KeyChar = (char)0;   //处理非法字符  
                }
            }
        }

        private void uiTextBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 0x20) e.KeyChar = (char)0;  //禁止空格键  
            if ((e.KeyChar == 0x2D) && (((TextBox)sender).Text.Length == 0)) return;   //处理负数  
            if (e.KeyChar > 0x20)
            {
                try
                {
                    double.Parse(((TextBox)sender).Text + e.KeyChar.ToString());
                }
                catch
                {
                    e.KeyChar = (char)0;   //处理非法字符  
                }
            }
        }

        private void uiTextBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 0x20) e.KeyChar = (char)0;  //禁止空格键  
            if ((e.KeyChar == 0x2D) && (((TextBox)sender).Text.Length == 0)) return;   //处理负数  
            if (e.KeyChar > 0x20)
            {
                try
                {
                    double.Parse(((TextBox)sender).Text + e.KeyChar.ToString());
                }
                catch
                {
                    e.KeyChar = (char)0;   //处理非法字符  
                }
            }
        }
        //点击重置按钮 所有数据归零
        private void uiButton1_Click(object sender, EventArgs e)
        {
            uiTextBox1.Text = "300";
            
            uiTextBox3.Text = "";
            uiTextBox4.Text = "0";
        }
        //点击计算按钮  计算收取费用
        private void uiButton2_Click(object sender, EventArgs e)
        {
            try
            {           
            //检查文本框是否为空
            
            int shoufei =Convert.ToInt32(uiTextBox1.Text.Trim());
            string banjixx = uiComboBox4.Text.Trim()+uiComboBox5.Text.Trim();
            int renshu = Convert.ToInt32(uiTextBox3.Text.Trim());
            int other = Convert.ToInt32(uiTextBox4.Text.Trim());
            int zongshu = shoufei * renshu + other;
            banji.Text = banjixx;
            jine.Text = zongshu.ToString();
             uiLabel6.Text = renshu.ToString();

            }
            catch
            {
                UIMessageBox.Show("检查所填数值！");
            }
        }
        // 收费信息加入表格
        private void JiaRu_button_Click(object sender, EventArgs e)
        {
            if (jine.Text=="")
            {
                return;
            }
            int index = this.uiDataGridView1.Rows.Add();            
            this.uiDataGridView1.Rows[index].Cells[0].Value = index+1;
            this.uiDataGridView1.Rows[index].Cells[1].Value = banji.Text;
            this.uiDataGridView1.Rows[index].Cells[2].Value = uiLabel6.Text;
            this.uiDataGridView1.Rows[index].Cells[3].Value = jine.Text;

        }
        //核心代码 统计学校所有数据信息
        private void uiButton3_Click(object sender, EventArgs e)
        {
            try
            {
                //uiDataGridView2.DataSource = null;// 清空
                uiDataGridView2.Rows.Clear();

                int BanJiShu = uiDataGridView1.Rows.Count;
                int XueShenShu=0, ZongShu=0;
                for (int i = 0; i < BanJiShu; i++)
                {
                    XueShenShu += Convert.ToInt32(uiDataGridView1.Rows[i].Cells[2].Value);
                    ZongShu+= Convert.ToInt32(uiDataGridView1.Rows[i].Cells[3].Value);
                }
                int ZongHeZhiChu = Convert.ToInt32(uiTextBox5.Text); //综合支出转换
                uiLabel19.Text = BanJiShu.ToString();  //显示班级数
                uiLabel20.Text = XueShenShu.ToString();//显示学生数
                uiLabel21.Text = ZongShu.ToString();//显示整体费用
                uiLabel22.Text = uiTextBox5.Text;//显示综合支出
                float GuanLiBiLi = Convert.ToInt32(uiComboBox1.Text);
                float KaoHeBiLi = Convert.ToInt32(uiComboBox2.Text);
                int YueShu = Convert.ToInt32(uiComboBox3.Text);

                float GuanLiZhiChu = (ZongShu - ZongHeZhiChu) * GuanLiBiLi/100;
                uiLabel24.Text = GuanLiZhiChu.ToString();//显示管理支出数额
                
                float MonthGuanLi = GuanLiZhiChu / YueShu;
                uiLabel26.Text = MonthGuanLi.ToString();//显示每月管理费用

                float JiaoXueKaoHe = (ZongShu - ZongHeZhiChu - GuanLiZhiChu) * KaoHeBiLi / 100;
                uiLabel28.Text = JiaoXueKaoHe.ToString();//显示考核基数

                float JiChuFaFang = (ZongShu - ZongHeZhiChu - GuanLiZhiChu) * (1-(KaoHeBiLi / 100));
                uiLabel30.Text = JiChuFaFang.ToString();  //显示每月正常发放数额

                float MonthFaFang = JiChuFaFang / YueShu;
                uiLabel32.Text = MonthFaFang.ToString();//显示每月发放
                float MeiSheng = MonthFaFang / XueShenShu;
                int KeChenShu = Convert.ToInt32(uiTextBox6.Text);
                for (int s = 0; s < BanJiShu; s++)
                {
                    int index = this.uiDataGridView2.Rows.Add();
                    uiDataGridView2.Rows[index].Cells[0].Value = uiDataGridView1.Rows[index].Cells[1].Value;
                    uiDataGridView2.Rows[index].Cells[1].Value = MeiSheng * Convert.ToInt32(uiDataGridView1.Rows[index].Cells[2].Value);
                    uiDataGridView2.Rows[index].Cells[2].Value = Convert.ToInt32(uiDataGridView2.Rows[index].Cells[1].Value) / KeChenShu;
                }           }
            catch
            {
                UIMessageBox.Show("发现错误？不应该呀！快快联系刘磊！QQ：247483085");
            }
           
        }

        private void uiTextBox5_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 0x20) e.KeyChar = (char)0;  //禁止空格键  
            if ((e.KeyChar == 0x2D) && (((TextBox)sender).Text.Length == 0)) return;   //处理负数  
            if (e.KeyChar > 0x20)
            {
                try
                {
                    double.Parse(((TextBox)sender).Text + e.KeyChar.ToString());
                }
                catch
                {
                    e.KeyChar = (char)0;   //处理非法字符  
                }
            }
        }
    }
}
