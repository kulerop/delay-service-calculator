﻿namespace 延时服务费用发放
{
    partial class form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(form1));
            this.uiTitlePanel1 = new Sunny.UI.UITitlePanel();
            this.uiComboBox5 = new Sunny.UI.UIComboBox();
            this.uiComboBox4 = new Sunny.UI.UIComboBox();
            this.uiLabel6 = new Sunny.UI.UILabel();
            this.uiDataGridView1 = new Sunny.UI.UIDataGridView();
            this.num = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bj = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rs = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.je = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.JiaRu_button = new Sunny.UI.UIButton();
            this.uiLabel8 = new Sunny.UI.UILabel();
            this.jine = new Sunny.UI.UILabel();
            this.banji = new Sunny.UI.UILabel();
            this.uiLabel5 = new Sunny.UI.UILabel();
            this.uiButton2 = new Sunny.UI.UIButton();
            this.uiButton1 = new Sunny.UI.UIButton();
            this.uiTextBox4 = new Sunny.UI.UITextBox();
            this.uiLabel4 = new Sunny.UI.UILabel();
            this.uiTextBox3 = new Sunny.UI.UITextBox();
            this.uiLabel3 = new Sunny.UI.UILabel();
            this.uiTextBox1 = new Sunny.UI.UITextBox();
            this.uiLabel2 = new Sunny.UI.UILabel();
            this.uiLabel1 = new Sunny.UI.UILabel();
            this.uiTitlePanel2 = new Sunny.UI.UITitlePanel();
            this.uiLabel36 = new Sunny.UI.UILabel();
            this.uiDataGridView2 = new Sunny.UI.UIDataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uiLabel32 = new Sunny.UI.UILabel();
            this.uiLabel33 = new Sunny.UI.UILabel();
            this.uiLabel30 = new Sunny.UI.UILabel();
            this.uiLabel31 = new Sunny.UI.UILabel();
            this.uiLabel28 = new Sunny.UI.UILabel();
            this.uiLabel29 = new Sunny.UI.UILabel();
            this.uiLabel26 = new Sunny.UI.UILabel();
            this.uiLabel27 = new Sunny.UI.UILabel();
            this.uiLabel24 = new Sunny.UI.UILabel();
            this.uiLabel25 = new Sunny.UI.UILabel();
            this.uiLabel22 = new Sunny.UI.UILabel();
            this.uiLabel23 = new Sunny.UI.UILabel();
            this.uiLabel21 = new Sunny.UI.UILabel();
            this.uiLabel20 = new Sunny.UI.UILabel();
            this.uiLabel19 = new Sunny.UI.UILabel();
            this.uiButton3 = new Sunny.UI.UIButton();
            this.uiLabel10 = new Sunny.UI.UILabel();
            this.uiLabel9 = new Sunny.UI.UILabel();
            this.uiLabel7 = new Sunny.UI.UILabel();
            this.uiTitlePanel3 = new Sunny.UI.UITitlePanel();
            this.uiLabel35 = new Sunny.UI.UILabel();
            this.uiTextBox6 = new Sunny.UI.UITextBox();
            this.uiLabel34 = new Sunny.UI.UILabel();
            this.uiComboBox3 = new Sunny.UI.UIComboBox();
            this.uiLabel18 = new Sunny.UI.UILabel();
            this.uiLabel17 = new Sunny.UI.UILabel();
            this.uiLabel16 = new Sunny.UI.UILabel();
            this.uiComboBox2 = new Sunny.UI.UIComboBox();
            this.uiLabel15 = new Sunny.UI.UILabel();
            this.uiComboBox1 = new Sunny.UI.UIComboBox();
            this.uiLabel14 = new Sunny.UI.UILabel();
            this.uiTextBox5 = new Sunny.UI.UITextBox();
            this.uiLabel13 = new Sunny.UI.UILabel();
            this.uiLabel12 = new Sunny.UI.UILabel();
            this.uiLabel11 = new Sunny.UI.UILabel();
            this.uiTitlePanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiDataGridView1)).BeginInit();
            this.uiTitlePanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiDataGridView2)).BeginInit();
            this.uiTitlePanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // uiTitlePanel1
            // 
            this.uiTitlePanel1.Controls.Add(this.uiComboBox5);
            this.uiTitlePanel1.Controls.Add(this.uiComboBox4);
            this.uiTitlePanel1.Controls.Add(this.uiLabel6);
            this.uiTitlePanel1.Controls.Add(this.uiDataGridView1);
            this.uiTitlePanel1.Controls.Add(this.JiaRu_button);
            this.uiTitlePanel1.Controls.Add(this.uiLabel8);
            this.uiTitlePanel1.Controls.Add(this.jine);
            this.uiTitlePanel1.Controls.Add(this.banji);
            this.uiTitlePanel1.Controls.Add(this.uiLabel5);
            this.uiTitlePanel1.Controls.Add(this.uiButton2);
            this.uiTitlePanel1.Controls.Add(this.uiButton1);
            this.uiTitlePanel1.Controls.Add(this.uiTextBox4);
            this.uiTitlePanel1.Controls.Add(this.uiLabel4);
            this.uiTitlePanel1.Controls.Add(this.uiTextBox3);
            this.uiTitlePanel1.Controls.Add(this.uiLabel3);
            this.uiTitlePanel1.Controls.Add(this.uiTextBox1);
            this.uiTitlePanel1.Controls.Add(this.uiLabel2);
            this.uiTitlePanel1.Controls.Add(this.uiLabel1);
            this.uiTitlePanel1.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.uiTitlePanel1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiTitlePanel1.ForeColor = System.Drawing.Color.White;
            this.uiTitlePanel1.Location = new System.Drawing.Point(4, 40);
            this.uiTitlePanel1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiTitlePanel1.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiTitlePanel1.Name = "uiTitlePanel1";
            this.uiTitlePanel1.Padding = new System.Windows.Forms.Padding(0, 35, 0, 0);
            this.uiTitlePanel1.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.uiTitlePanel1.Size = new System.Drawing.Size(950, 388);
            this.uiTitlePanel1.Style = Sunny.UI.UIStyle.Red;
            this.uiTitlePanel1.TabIndex = 0;
            this.uiTitlePanel1.Text = "第一步：费用收取";
            this.uiTitlePanel1.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            // 
            // uiComboBox5
            // 
            this.uiComboBox5.FillColor = System.Drawing.Color.White;
            this.uiComboBox5.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiComboBox5.Location = new System.Drawing.Point(253, 112);
            this.uiComboBox5.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiComboBox5.MinimumSize = new System.Drawing.Size(63, 0);
            this.uiComboBox5.Name = "uiComboBox5";
            this.uiComboBox5.Padding = new System.Windows.Forms.Padding(0, 0, 30, 2);
            this.uiComboBox5.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.uiComboBox5.Size = new System.Drawing.Size(65, 34);
            this.uiComboBox5.Style = Sunny.UI.UIStyle.Red;
            this.uiComboBox5.TabIndex = 18;
            this.uiComboBox5.Text = "1班";
            this.uiComboBox5.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiComboBox4
            // 
            this.uiComboBox4.FillColor = System.Drawing.Color.White;
            this.uiComboBox4.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiComboBox4.Location = new System.Drawing.Point(147, 112);
            this.uiComboBox4.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiComboBox4.MinimumSize = new System.Drawing.Size(63, 0);
            this.uiComboBox4.Name = "uiComboBox4";
            this.uiComboBox4.Padding = new System.Windows.Forms.Padding(0, 0, 30, 2);
            this.uiComboBox4.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.uiComboBox4.Size = new System.Drawing.Size(95, 34);
            this.uiComboBox4.Style = Sunny.UI.UIStyle.Red;
            this.uiComboBox4.TabIndex = 17;
            this.uiComboBox4.Text = "一年级";
            this.uiComboBox4.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel6
            // 
            this.uiLabel6.BackColor = System.Drawing.Color.Transparent;
            this.uiLabel6.Font = new System.Drawing.Font("微软雅黑", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiLabel6.Location = new System.Drawing.Point(483, 53);
            this.uiLabel6.Name = "uiLabel6";
            this.uiLabel6.Size = new System.Drawing.Size(46, 39);
            this.uiLabel6.Style = Sunny.UI.UIStyle.Red;
            this.uiLabel6.TabIndex = 16;
            this.uiLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiDataGridView1
            // 
            this.uiDataGridView1.AllowUserToAddRows = false;
            this.uiDataGridView1.AllowUserToResizeColumns = false;
            this.uiDataGridView1.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.uiDataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.uiDataGridView1.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.uiDataGridView1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("微软雅黑", 12F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.uiDataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.uiDataGridView1.ColumnHeadersHeight = 32;
            this.uiDataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.uiDataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.num,
            this.bj,
            this.rs,
            this.je});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("微软雅黑", 12F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(160)))), ((int)(((byte)(160)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.uiDataGridView1.DefaultCellStyle = dataGridViewCellStyle3;
            this.uiDataGridView1.EnableHeadersVisualStyles = false;
            this.uiDataGridView1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiDataGridView1.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.uiDataGridView1.Location = new System.Drawing.Point(382, 121);
            this.uiDataGridView1.Name = "uiDataGridView1";
            this.uiDataGridView1.ReadOnly = true;
            this.uiDataGridView1.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("微软雅黑", 12F);
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.uiDataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.uiDataGridView1.RowHeadersWidth = 51;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            this.uiDataGridView1.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.uiDataGridView1.RowTemplate.Height = 29;
            this.uiDataGridView1.SelectedIndex = -1;
            this.uiDataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.uiDataGridView1.ShowGridLine = true;
            this.uiDataGridView1.Size = new System.Drawing.Size(554, 249);
            this.uiDataGridView1.StripeOddColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.uiDataGridView1.Style = Sunny.UI.UIStyle.Red;
            this.uiDataGridView1.TabIndex = 15;
            // 
            // num
            // 
            this.num.HeaderText = "序号";
            this.num.MinimumWidth = 6;
            this.num.Name = "num";
            this.num.ReadOnly = true;
            this.num.Width = 125;
            // 
            // bj
            // 
            this.bj.HeaderText = "班级";
            this.bj.MinimumWidth = 6;
            this.bj.Name = "bj";
            this.bj.ReadOnly = true;
            this.bj.Width = 125;
            // 
            // rs
            // 
            this.rs.HeaderText = "人数";
            this.rs.MinimumWidth = 6;
            this.rs.Name = "rs";
            this.rs.ReadOnly = true;
            this.rs.Width = 125;
            // 
            // je
            // 
            this.je.HeaderText = "总收入";
            this.je.MinimumWidth = 6;
            this.je.Name = "je";
            this.je.ReadOnly = true;
            this.je.Width = 125;
            // 
            // JiaRu_button
            // 
            this.JiaRu_button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.JiaRu_button.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.JiaRu_button.FillHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(127)))), ((int)(((byte)(128)))));
            this.JiaRu_button.FillPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.JiaRu_button.FillSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.JiaRu_button.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.JiaRu_button.Location = new System.Drawing.Point(821, 55);
            this.JiaRu_button.MinimumSize = new System.Drawing.Size(1, 1);
            this.JiaRu_button.Name = "JiaRu_button";
            this.JiaRu_button.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.JiaRu_button.RectHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(127)))), ((int)(((byte)(128)))));
            this.JiaRu_button.RectPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.JiaRu_button.RectSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.JiaRu_button.Size = new System.Drawing.Size(115, 33);
            this.JiaRu_button.Style = Sunny.UI.UIStyle.Red;
            this.JiaRu_button.TabIndex = 14;
            this.JiaRu_button.Text = "加入列表";
            this.JiaRu_button.Click += new System.EventHandler(this.JiaRu_button_Click);
            // 
            // uiLabel8
            // 
            this.uiLabel8.BackColor = System.Drawing.Color.Transparent;
            this.uiLabel8.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel8.Location = new System.Drawing.Point(769, 52);
            this.uiLabel8.Name = "uiLabel8";
            this.uiLabel8.Size = new System.Drawing.Size(78, 39);
            this.uiLabel8.Style = Sunny.UI.UIStyle.Red;
            this.uiLabel8.TabIndex = 13;
            this.uiLabel8.Text = "元";
            this.uiLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // jine
            // 
            this.jine.BackColor = System.Drawing.Color.Transparent;
            this.jine.Font = new System.Drawing.Font("微软雅黑", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.jine.Location = new System.Drawing.Point(680, 52);
            this.jine.Name = "jine";
            this.jine.Size = new System.Drawing.Size(83, 39);
            this.jine.Style = Sunny.UI.UIStyle.Red;
            this.jine.TabIndex = 12;
            this.jine.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // banji
            // 
            this.banji.BackColor = System.Drawing.Color.Transparent;
            this.banji.Font = new System.Drawing.Font("微软雅黑", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.banji.Location = new System.Drawing.Point(363, 53);
            this.banji.Name = "banji";
            this.banji.Size = new System.Drawing.Size(114, 39);
            this.banji.Style = Sunny.UI.UIStyle.Red;
            this.banji.TabIndex = 11;
            this.banji.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel5
            // 
            this.uiLabel5.BackColor = System.Drawing.Color.Transparent;
            this.uiLabel5.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel5.Location = new System.Drawing.Point(535, 53);
            this.uiLabel5.Name = "uiLabel5";
            this.uiLabel5.Size = new System.Drawing.Size(134, 39);
            this.uiLabel5.Style = Sunny.UI.UIStyle.Red;
            this.uiLabel5.TabIndex = 10;
            this.uiLabel5.Text = "人共收取费用";
            this.uiLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiButton2
            // 
            this.uiButton2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.uiButton2.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.uiButton2.FillHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(127)))), ((int)(((byte)(128)))));
            this.uiButton2.FillPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.uiButton2.FillSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.uiButton2.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiButton2.Location = new System.Drawing.Point(193, 299);
            this.uiButton2.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiButton2.Name = "uiButton2";
            this.uiButton2.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.uiButton2.RectHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(127)))), ((int)(((byte)(128)))));
            this.uiButton2.RectPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.uiButton2.RectSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.uiButton2.Size = new System.Drawing.Size(125, 37);
            this.uiButton2.Style = Sunny.UI.UIStyle.Red;
            this.uiButton2.TabIndex = 9;
            this.uiButton2.Text = "计算";
            this.uiButton2.Click += new System.EventHandler(this.uiButton2_Click);
            // 
            // uiButton1
            // 
            this.uiButton1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.uiButton1.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.uiButton1.FillHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(127)))), ((int)(((byte)(128)))));
            this.uiButton1.FillPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.uiButton1.FillSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.uiButton1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiButton1.Location = new System.Drawing.Point(31, 299);
            this.uiButton1.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiButton1.Name = "uiButton1";
            this.uiButton1.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.uiButton1.RectHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(127)))), ((int)(((byte)(128)))));
            this.uiButton1.RectPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.uiButton1.RectSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.uiButton1.Size = new System.Drawing.Size(125, 37);
            this.uiButton1.Style = Sunny.UI.UIStyle.Red;
            this.uiButton1.TabIndex = 8;
            this.uiButton1.Text = "重置";
            this.uiButton1.Click += new System.EventHandler(this.uiButton1_Click);
            // 
            // uiTextBox4
            // 
            this.uiTextBox4.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.uiTextBox4.FillColor = System.Drawing.Color.White;
            this.uiTextBox4.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiTextBox4.Location = new System.Drawing.Point(147, 238);
            this.uiTextBox4.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiTextBox4.Maximum = 2147483647D;
            this.uiTextBox4.Minimum = -2147483648D;
            this.uiTextBox4.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiTextBox4.Name = "uiTextBox4";
            this.uiTextBox4.Padding = new System.Windows.Forms.Padding(5);
            this.uiTextBox4.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.uiTextBox4.Size = new System.Drawing.Size(171, 34);
            this.uiTextBox4.Style = Sunny.UI.UIStyle.Red;
            this.uiTextBox4.TabIndex = 7;
            this.uiTextBox4.Text = "0";
            this.uiTextBox4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.uiTextBox4_KeyPress);
            // 
            // uiLabel4
            // 
            this.uiLabel4.BackColor = System.Drawing.Color.Transparent;
            this.uiLabel4.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel4.Location = new System.Drawing.Point(26, 234);
            this.uiLabel4.Name = "uiLabel4";
            this.uiLabel4.Size = new System.Drawing.Size(120, 43);
            this.uiLabel4.Style = Sunny.UI.UIStyle.Red;
            this.uiLabel4.TabIndex = 6;
            this.uiLabel4.Text = "其他费用";
            this.uiLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiTextBox3
            // 
            this.uiTextBox3.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.uiTextBox3.FillColor = System.Drawing.Color.White;
            this.uiTextBox3.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiTextBox3.Location = new System.Drawing.Point(147, 175);
            this.uiTextBox3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiTextBox3.Maximum = 2147483647D;
            this.uiTextBox3.Minimum = -2147483648D;
            this.uiTextBox3.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiTextBox3.Name = "uiTextBox3";
            this.uiTextBox3.Padding = new System.Windows.Forms.Padding(5);
            this.uiTextBox3.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.uiTextBox3.Size = new System.Drawing.Size(171, 34);
            this.uiTextBox3.Style = Sunny.UI.UIStyle.Red;
            this.uiTextBox3.TabIndex = 5;
            this.uiTextBox3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.uiTextBox3_KeyPress);
            // 
            // uiLabel3
            // 
            this.uiLabel3.BackColor = System.Drawing.Color.Transparent;
            this.uiLabel3.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel3.Location = new System.Drawing.Point(26, 171);
            this.uiLabel3.Name = "uiLabel3";
            this.uiLabel3.Size = new System.Drawing.Size(120, 43);
            this.uiLabel3.Style = Sunny.UI.UIStyle.Red;
            this.uiLabel3.TabIndex = 4;
            this.uiLabel3.Text = "收费人数";
            this.uiLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiTextBox1
            // 
            this.uiTextBox1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.uiTextBox1.DoubleValue = 300D;
            this.uiTextBox1.FillColor = System.Drawing.Color.White;
            this.uiTextBox1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiTextBox1.IntValue = 300;
            this.uiTextBox1.Location = new System.Drawing.Point(147, 52);
            this.uiTextBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiTextBox1.Maximum = 2147483647D;
            this.uiTextBox1.Minimum = -2147483648D;
            this.uiTextBox1.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiTextBox1.Name = "uiTextBox1";
            this.uiTextBox1.Padding = new System.Windows.Forms.Padding(5);
            this.uiTextBox1.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.uiTextBox1.Size = new System.Drawing.Size(171, 34);
            this.uiTextBox1.Style = Sunny.UI.UIStyle.Red;
            this.uiTextBox1.TabIndex = 2;
            this.uiTextBox1.Text = "300";
            this.uiTextBox1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.uiTextBox1_KeyPress);
            // 
            // uiLabel2
            // 
            this.uiLabel2.BackColor = System.Drawing.Color.Transparent;
            this.uiLabel2.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel2.Location = new System.Drawing.Point(26, 108);
            this.uiLabel2.Name = "uiLabel2";
            this.uiLabel2.Size = new System.Drawing.Size(120, 43);
            this.uiLabel2.Style = Sunny.UI.UIStyle.Red;
            this.uiLabel2.TabIndex = 1;
            this.uiLabel2.Text = "班级信息";
            this.uiLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel1
            // 
            this.uiLabel1.BackColor = System.Drawing.Color.Transparent;
            this.uiLabel1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel1.Location = new System.Drawing.Point(26, 50);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Size = new System.Drawing.Size(120, 38);
            this.uiLabel1.Style = Sunny.UI.UIStyle.Red;
            this.uiLabel1.TabIndex = 0;
            this.uiLabel1.Text = "生均收费";
            this.uiLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiTitlePanel2
            // 
            this.uiTitlePanel2.Controls.Add(this.uiLabel36);
            this.uiTitlePanel2.Controls.Add(this.uiDataGridView2);
            this.uiTitlePanel2.Controls.Add(this.uiLabel32);
            this.uiTitlePanel2.Controls.Add(this.uiLabel33);
            this.uiTitlePanel2.Controls.Add(this.uiLabel30);
            this.uiTitlePanel2.Controls.Add(this.uiLabel31);
            this.uiTitlePanel2.Controls.Add(this.uiLabel28);
            this.uiTitlePanel2.Controls.Add(this.uiLabel29);
            this.uiTitlePanel2.Controls.Add(this.uiLabel26);
            this.uiTitlePanel2.Controls.Add(this.uiLabel27);
            this.uiTitlePanel2.Controls.Add(this.uiLabel24);
            this.uiTitlePanel2.Controls.Add(this.uiLabel25);
            this.uiTitlePanel2.Controls.Add(this.uiLabel22);
            this.uiTitlePanel2.Controls.Add(this.uiLabel23);
            this.uiTitlePanel2.Controls.Add(this.uiLabel21);
            this.uiTitlePanel2.Controls.Add(this.uiLabel20);
            this.uiTitlePanel2.Controls.Add(this.uiLabel19);
            this.uiTitlePanel2.Controls.Add(this.uiButton3);
            this.uiTitlePanel2.Controls.Add(this.uiLabel10);
            this.uiTitlePanel2.Controls.Add(this.uiLabel9);
            this.uiTitlePanel2.Controls.Add(this.uiLabel7);
            this.uiTitlePanel2.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.uiTitlePanel2.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiTitlePanel2.ForeColor = System.Drawing.Color.White;
            this.uiTitlePanel2.Location = new System.Drawing.Point(967, 40);
            this.uiTitlePanel2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiTitlePanel2.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiTitlePanel2.Name = "uiTitlePanel2";
            this.uiTitlePanel2.Padding = new System.Windows.Forms.Padding(0, 35, 0, 0);
            this.uiTitlePanel2.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.uiTitlePanel2.Size = new System.Drawing.Size(412, 684);
            this.uiTitlePanel2.Style = Sunny.UI.UIStyle.Red;
            this.uiTitlePanel2.TabIndex = 1;
            this.uiTitlePanel2.Text = "第三步：计算学校总收费信息";
            this.uiTitlePanel2.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            // 
            // uiLabel36
            // 
            this.uiLabel36.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel36.Location = new System.Drawing.Point(234, 605);
            this.uiLabel36.Name = "uiLabel36";
            this.uiLabel36.Size = new System.Drawing.Size(174, 63);
            this.uiLabel36.Style = Sunny.UI.UIStyle.Red;
            this.uiLabel36.TabIndex = 34;
            this.uiLabel36.Text = "可选中复制表格\r\n粘贴到excel";
            this.uiLabel36.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiDataGridView2
            // 
            this.uiDataGridView2.AllowUserToAddRows = false;
            this.uiDataGridView2.AllowUserToDeleteRows = false;
            this.uiDataGridView2.AllowUserToResizeColumns = false;
            this.uiDataGridView2.AllowUserToResizeRows = false;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            this.uiDataGridView2.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle6;
            this.uiDataGridView2.BackgroundColor = System.Drawing.Color.PapayaWhip;
            this.uiDataGridView2.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            dataGridViewCellStyle7.Font = new System.Drawing.Font("微软雅黑", 12F);
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.uiDataGridView2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.uiDataGridView2.ColumnHeadersHeight = 32;
            this.uiDataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.uiDataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3});
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("微软雅黑", 12F);
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(160)))), ((int)(((byte)(160)))));
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.uiDataGridView2.DefaultCellStyle = dataGridViewCellStyle8;
            this.uiDataGridView2.EnableHeadersVisualStyles = false;
            this.uiDataGridView2.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiDataGridView2.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.uiDataGridView2.Location = new System.Drawing.Point(3, 366);
            this.uiDataGridView2.Name = "uiDataGridView2";
            this.uiDataGridView2.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            dataGridViewCellStyle9.Font = new System.Drawing.Font("微软雅黑", 12F);
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.uiDataGridView2.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.uiDataGridView2.RowHeadersWidth = 51;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.White;
            this.uiDataGridView2.RowsDefaultCellStyle = dataGridViewCellStyle10;
            this.uiDataGridView2.RowTemplate.Height = 29;
            this.uiDataGridView2.SelectedIndex = -1;
            this.uiDataGridView2.ShowGridLine = true;
            this.uiDataGridView2.Size = new System.Drawing.Size(406, 239);
            this.uiDataGridView2.StripeOddColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.uiDataGridView2.Style = Sunny.UI.UIStyle.Red;
            this.uiDataGridView2.TabIndex = 33;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "班级";
            this.Column1.MinimumWidth = 6;
            this.Column1.Name = "Column1";
            this.Column1.Width = 125;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "月总额";
            this.Column2.MinimumWidth = 6;
            this.Column2.Name = "Column2";
            this.Column2.Width = 125;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "每课";
            this.Column3.MinimumWidth = 6;
            this.Column3.Name = "Column3";
            this.Column3.Width = 125;
            // 
            // uiLabel32
            // 
            this.uiLabel32.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel32.Location = new System.Drawing.Point(193, 320);
            this.uiLabel32.Name = "uiLabel32";
            this.uiLabel32.Size = new System.Drawing.Size(168, 39);
            this.uiLabel32.Style = Sunny.UI.UIStyle.Red;
            this.uiLabel32.TabIndex = 32;
            this.uiLabel32.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel33
            // 
            this.uiLabel33.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel33.Location = new System.Drawing.Point(21, 320);
            this.uiLabel33.Name = "uiLabel33";
            this.uiLabel33.Size = new System.Drawing.Size(152, 39);
            this.uiLabel33.Style = Sunny.UI.UIStyle.Red;
            this.uiLabel33.TabIndex = 31;
            this.uiLabel33.Text = "每月发放数额：";
            this.uiLabel33.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel30
            // 
            this.uiLabel30.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel30.Location = new System.Drawing.Point(193, 281);
            this.uiLabel30.Name = "uiLabel30";
            this.uiLabel30.Size = new System.Drawing.Size(168, 39);
            this.uiLabel30.Style = Sunny.UI.UIStyle.Red;
            this.uiLabel30.TabIndex = 30;
            this.uiLabel30.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel31
            // 
            this.uiLabel31.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel31.Location = new System.Drawing.Point(21, 281);
            this.uiLabel31.Name = "uiLabel31";
            this.uiLabel31.Size = new System.Drawing.Size(152, 39);
            this.uiLabel31.Style = Sunny.UI.UIStyle.Red;
            this.uiLabel31.TabIndex = 29;
            this.uiLabel31.Text = "基础发放总额：";
            this.uiLabel31.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel28
            // 
            this.uiLabel28.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel28.Location = new System.Drawing.Point(193, 242);
            this.uiLabel28.Name = "uiLabel28";
            this.uiLabel28.Size = new System.Drawing.Size(168, 39);
            this.uiLabel28.Style = Sunny.UI.UIStyle.Red;
            this.uiLabel28.TabIndex = 28;
            this.uiLabel28.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel29
            // 
            this.uiLabel29.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel29.Location = new System.Drawing.Point(21, 242);
            this.uiLabel29.Name = "uiLabel29";
            this.uiLabel29.Size = new System.Drawing.Size(152, 39);
            this.uiLabel29.Style = Sunny.UI.UIStyle.Red;
            this.uiLabel29.TabIndex = 27;
            this.uiLabel29.Text = "教学考核总数：";
            this.uiLabel29.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel26
            // 
            this.uiLabel26.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel26.Location = new System.Drawing.Point(193, 203);
            this.uiLabel26.Name = "uiLabel26";
            this.uiLabel26.Size = new System.Drawing.Size(168, 39);
            this.uiLabel26.Style = Sunny.UI.UIStyle.Red;
            this.uiLabel26.TabIndex = 26;
            this.uiLabel26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel27
            // 
            this.uiLabel27.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel27.Location = new System.Drawing.Point(21, 203);
            this.uiLabel27.Name = "uiLabel27";
            this.uiLabel27.Size = new System.Drawing.Size(152, 39);
            this.uiLabel27.Style = Sunny.UI.UIStyle.Red;
            this.uiLabel27.TabIndex = 25;
            this.uiLabel27.Text = "每月管理费用：";
            this.uiLabel27.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel24
            // 
            this.uiLabel24.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel24.Location = new System.Drawing.Point(193, 164);
            this.uiLabel24.Name = "uiLabel24";
            this.uiLabel24.Size = new System.Drawing.Size(168, 39);
            this.uiLabel24.Style = Sunny.UI.UIStyle.Red;
            this.uiLabel24.TabIndex = 24;
            this.uiLabel24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel25
            // 
            this.uiLabel25.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel25.Location = new System.Drawing.Point(21, 164);
            this.uiLabel25.Name = "uiLabel25";
            this.uiLabel25.Size = new System.Drawing.Size(152, 39);
            this.uiLabel25.Style = Sunny.UI.UIStyle.Red;
            this.uiLabel25.TabIndex = 23;
            this.uiLabel25.Text = "学期管理支出：";
            this.uiLabel25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel22
            // 
            this.uiLabel22.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel22.Location = new System.Drawing.Point(157, 125);
            this.uiLabel22.Name = "uiLabel22";
            this.uiLabel22.Size = new System.Drawing.Size(168, 39);
            this.uiLabel22.Style = Sunny.UI.UIStyle.Red;
            this.uiLabel22.TabIndex = 22;
            this.uiLabel22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel23
            // 
            this.uiLabel23.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel23.Location = new System.Drawing.Point(21, 125);
            this.uiLabel23.Name = "uiLabel23";
            this.uiLabel23.Size = new System.Drawing.Size(138, 39);
            this.uiLabel23.Style = Sunny.UI.UIStyle.Red;
            this.uiLabel23.TabIndex = 21;
            this.uiLabel23.Text = "综合支出：";
            this.uiLabel23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel21
            // 
            this.uiLabel21.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel21.Location = new System.Drawing.Point(157, 86);
            this.uiLabel21.Name = "uiLabel21";
            this.uiLabel21.Size = new System.Drawing.Size(182, 39);
            this.uiLabel21.Style = Sunny.UI.UIStyle.Red;
            this.uiLabel21.TabIndex = 20;
            this.uiLabel21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel20
            // 
            this.uiLabel20.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel20.Location = new System.Drawing.Point(312, 47);
            this.uiLabel20.Name = "uiLabel20";
            this.uiLabel20.Size = new System.Drawing.Size(71, 39);
            this.uiLabel20.Style = Sunny.UI.UIStyle.Red;
            this.uiLabel20.TabIndex = 19;
            this.uiLabel20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel19
            // 
            this.uiLabel19.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel19.Location = new System.Drawing.Point(142, 47);
            this.uiLabel19.Name = "uiLabel19";
            this.uiLabel19.Size = new System.Drawing.Size(57, 39);
            this.uiLabel19.Style = Sunny.UI.UIStyle.Red;
            this.uiLabel19.TabIndex = 18;
            this.uiLabel19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiButton3
            // 
            this.uiButton3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.uiButton3.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.uiButton3.FillHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(127)))), ((int)(((byte)(128)))));
            this.uiButton3.FillPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.uiButton3.FillSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.uiButton3.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiButton3.Location = new System.Drawing.Point(15, 611);
            this.uiButton3.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiButton3.Name = "uiButton3";
            this.uiButton3.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.uiButton3.RectHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(127)))), ((int)(((byte)(128)))));
            this.uiButton3.RectPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.uiButton3.RectSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.uiButton3.Size = new System.Drawing.Size(197, 56);
            this.uiButton3.Style = Sunny.UI.UIStyle.Red;
            this.uiButton3.TabIndex = 17;
            this.uiButton3.Text = "整体计算";
            this.uiButton3.Click += new System.EventHandler(this.uiButton3_Click);
            // 
            // uiLabel10
            // 
            this.uiLabel10.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel10.Location = new System.Drawing.Point(21, 86);
            this.uiLabel10.Name = "uiLabel10";
            this.uiLabel10.Size = new System.Drawing.Size(138, 39);
            this.uiLabel10.Style = Sunny.UI.UIStyle.Red;
            this.uiLabel10.TabIndex = 2;
            this.uiLabel10.Text = "收取总金额：";
            this.uiLabel10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel9
            // 
            this.uiLabel9.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel9.Location = new System.Drawing.Point(193, 47);
            this.uiLabel9.Name = "uiLabel9";
            this.uiLabel9.Size = new System.Drawing.Size(132, 39);
            this.uiLabel9.Style = Sunny.UI.UIStyle.Red;
            this.uiLabel9.TabIndex = 1;
            this.uiLabel9.Text = "参加学生数：";
            this.uiLabel9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel7
            // 
            this.uiLabel7.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel7.Location = new System.Drawing.Point(21, 47);
            this.uiLabel7.Name = "uiLabel7";
            this.uiLabel7.Size = new System.Drawing.Size(138, 39);
            this.uiLabel7.Style = Sunny.UI.UIStyle.Red;
            this.uiLabel7.TabIndex = 0;
            this.uiLabel7.Text = "学校班级数：";
            this.uiLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiTitlePanel3
            // 
            this.uiTitlePanel3.Controls.Add(this.uiLabel35);
            this.uiTitlePanel3.Controls.Add(this.uiTextBox6);
            this.uiTitlePanel3.Controls.Add(this.uiLabel34);
            this.uiTitlePanel3.Controls.Add(this.uiComboBox3);
            this.uiTitlePanel3.Controls.Add(this.uiLabel18);
            this.uiTitlePanel3.Controls.Add(this.uiLabel17);
            this.uiTitlePanel3.Controls.Add(this.uiLabel16);
            this.uiTitlePanel3.Controls.Add(this.uiComboBox2);
            this.uiTitlePanel3.Controls.Add(this.uiLabel15);
            this.uiTitlePanel3.Controls.Add(this.uiComboBox1);
            this.uiTitlePanel3.Controls.Add(this.uiLabel14);
            this.uiTitlePanel3.Controls.Add(this.uiTextBox5);
            this.uiTitlePanel3.Controls.Add(this.uiLabel13);
            this.uiTitlePanel3.Controls.Add(this.uiLabel12);
            this.uiTitlePanel3.Controls.Add(this.uiLabel11);
            this.uiTitlePanel3.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.uiTitlePanel3.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiTitlePanel3.ForeColor = System.Drawing.Color.White;
            this.uiTitlePanel3.Location = new System.Drawing.Point(8, 440);
            this.uiTitlePanel3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiTitlePanel3.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiTitlePanel3.Name = "uiTitlePanel3";
            this.uiTitlePanel3.Padding = new System.Windows.Forms.Padding(0, 35, 0, 0);
            this.uiTitlePanel3.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.uiTitlePanel3.Size = new System.Drawing.Size(946, 284);
            this.uiTitlePanel3.Style = Sunny.UI.UIStyle.Red;
            this.uiTitlePanel3.TabIndex = 2;
            this.uiTitlePanel3.Text = "第二步：学校支出";
            this.uiTitlePanel3.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            // 
            // uiLabel35
            // 
            this.uiLabel35.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel35.Location = new System.Drawing.Point(485, 225);
            this.uiLabel35.Name = "uiLabel35";
            this.uiLabel35.Size = new System.Drawing.Size(446, 58);
            this.uiLabel35.Style = Sunny.UI.UIStyle.Red;
            this.uiLabel35.TabIndex = 14;
            this.uiLabel35.Text = "计算出每课费用后，乘以本月上课节数就是本月应该拿的服务费。";
            this.uiLabel35.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiTextBox6
            // 
            this.uiTextBox6.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.uiTextBox6.DoubleValue = 20D;
            this.uiTextBox6.FillColor = System.Drawing.Color.White;
            this.uiTextBox6.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiTextBox6.IntValue = 20;
            this.uiTextBox6.Location = new System.Drawing.Point(223, 225);
            this.uiTextBox6.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiTextBox6.Maximum = 2147483647D;
            this.uiTextBox6.Minimum = -2147483648D;
            this.uiTextBox6.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiTextBox6.Name = "uiTextBox6";
            this.uiTextBox6.Padding = new System.Windows.Forms.Padding(5);
            this.uiTextBox6.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.uiTextBox6.Size = new System.Drawing.Size(125, 34);
            this.uiTextBox6.Style = Sunny.UI.UIStyle.Red;
            this.uiTextBox6.TabIndex = 13;
            this.uiTextBox6.Text = "20";
            // 
            // uiLabel34
            // 
            this.uiLabel34.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel34.Location = new System.Drawing.Point(25, 220);
            this.uiLabel34.Name = "uiLabel34";
            this.uiLabel34.Size = new System.Drawing.Size(201, 43);
            this.uiLabel34.Style = Sunny.UI.UIStyle.Red;
            this.uiLabel34.TabIndex = 12;
            this.uiLabel34.Text = "每月辅导数量：";
            this.uiLabel34.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiComboBox3
            // 
            this.uiComboBox3.FillColor = System.Drawing.Color.White;
            this.uiComboBox3.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiComboBox3.Location = new System.Drawing.Point(223, 181);
            this.uiComboBox3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiComboBox3.MinimumSize = new System.Drawing.Size(63, 0);
            this.uiComboBox3.Name = "uiComboBox3";
            this.uiComboBox3.Padding = new System.Windows.Forms.Padding(0, 0, 30, 2);
            this.uiComboBox3.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.uiComboBox3.Size = new System.Drawing.Size(127, 34);
            this.uiComboBox3.Style = Sunny.UI.UIStyle.Red;
            this.uiComboBox3.TabIndex = 11;
            this.uiComboBox3.Text = "4";
            this.uiComboBox3.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel18
            // 
            this.uiLabel18.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel18.Location = new System.Drawing.Point(25, 177);
            this.uiLabel18.Name = "uiLabel18";
            this.uiLabel18.Size = new System.Drawing.Size(201, 43);
            this.uiLabel18.Style = Sunny.UI.UIStyle.Red;
            this.uiLabel18.TabIndex = 10;
            this.uiLabel18.Text = "本学期共有几月：";
            this.uiLabel18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel17
            // 
            this.uiLabel17.Font = new System.Drawing.Font("华文新魏", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiLabel17.Location = new System.Drawing.Point(480, 46);
            this.uiLabel17.Name = "uiLabel17";
            this.uiLabel17.Size = new System.Drawing.Size(448, 179);
            this.uiLabel17.Style = Sunny.UI.UIStyle.Red;
            this.uiLabel17.TabIndex = 9;
            this.uiLabel17.Text = "计算公式为： （学校总收入 -综合支出-学校管理支出-考核奖金）/总学生数/本学期月份*班级学生数=班级每月总辅导费\r\n班级总辅导费/总晚辅导节数=本班级每节课费" +
    "用\r\n每节课费用*本月老师所上节数=教师本月应得辅导费用";
            this.uiLabel17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel16
            // 
            this.uiLabel16.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel16.Location = new System.Drawing.Point(355, 136);
            this.uiLabel16.Name = "uiLabel16";
            this.uiLabel16.Size = new System.Drawing.Size(67, 34);
            this.uiLabel16.Style = Sunny.UI.UIStyle.Red;
            this.uiLabel16.TabIndex = 8;
            this.uiLabel16.Text = "%";
            this.uiLabel16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiComboBox2
            // 
            this.uiComboBox2.FillColor = System.Drawing.Color.White;
            this.uiComboBox2.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiComboBox2.Location = new System.Drawing.Point(223, 136);
            this.uiComboBox2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiComboBox2.MinimumSize = new System.Drawing.Size(63, 0);
            this.uiComboBox2.Name = "uiComboBox2";
            this.uiComboBox2.Padding = new System.Windows.Forms.Padding(0, 0, 30, 2);
            this.uiComboBox2.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.uiComboBox2.Size = new System.Drawing.Size(127, 34);
            this.uiComboBox2.Style = Sunny.UI.UIStyle.Red;
            this.uiComboBox2.TabIndex = 7;
            this.uiComboBox2.Text = "20";
            this.uiComboBox2.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel15
            // 
            this.uiLabel15.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel15.Location = new System.Drawing.Point(355, 98);
            this.uiLabel15.Name = "uiLabel15";
            this.uiLabel15.Size = new System.Drawing.Size(67, 34);
            this.uiLabel15.Style = Sunny.UI.UIStyle.Red;
            this.uiLabel15.TabIndex = 6;
            this.uiLabel15.Text = "%";
            this.uiLabel15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiComboBox1
            // 
            this.uiComboBox1.FillColor = System.Drawing.Color.White;
            this.uiComboBox1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiComboBox1.Location = new System.Drawing.Point(226, 93);
            this.uiComboBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiComboBox1.MinimumSize = new System.Drawing.Size(63, 0);
            this.uiComboBox1.Name = "uiComboBox1";
            this.uiComboBox1.Padding = new System.Windows.Forms.Padding(0, 0, 30, 2);
            this.uiComboBox1.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.uiComboBox1.Size = new System.Drawing.Size(127, 34);
            this.uiComboBox1.Style = Sunny.UI.UIStyle.Red;
            this.uiComboBox1.TabIndex = 5;
            this.uiComboBox1.Text = "5";
            this.uiComboBox1.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel14
            // 
            this.uiLabel14.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel14.Location = new System.Drawing.Point(355, 45);
            this.uiLabel14.Name = "uiLabel14";
            this.uiLabel14.Size = new System.Drawing.Size(100, 43);
            this.uiLabel14.Style = Sunny.UI.UIStyle.Red;
            this.uiLabel14.TabIndex = 4;
            this.uiLabel14.Text = "（元）";
            this.uiLabel14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiTextBox5
            // 
            this.uiTextBox5.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.uiTextBox5.FillColor = System.Drawing.Color.White;
            this.uiTextBox5.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiTextBox5.Location = new System.Drawing.Point(225, 50);
            this.uiTextBox5.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiTextBox5.Maximum = 2147483647D;
            this.uiTextBox5.Minimum = -2147483648D;
            this.uiTextBox5.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiTextBox5.Name = "uiTextBox5";
            this.uiTextBox5.Padding = new System.Windows.Forms.Padding(5);
            this.uiTextBox5.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.uiTextBox5.Size = new System.Drawing.Size(125, 34);
            this.uiTextBox5.Style = Sunny.UI.UIStyle.Red;
            this.uiTextBox5.TabIndex = 3;
            this.uiTextBox5.Text = "0";
            this.uiTextBox5.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.uiTextBox5_KeyPress);
            // 
            // uiLabel13
            // 
            this.uiLabel13.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel13.Location = new System.Drawing.Point(25, 132);
            this.uiLabel13.Name = "uiLabel13";
            this.uiLabel13.Size = new System.Drawing.Size(201, 43);
            this.uiLabel13.Style = Sunny.UI.UIStyle.Red;
            this.uiLabel13.TabIndex = 2;
            this.uiLabel13.Text = "教学考核部分比例：";
            this.uiLabel13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel12
            // 
            this.uiLabel12.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel12.Location = new System.Drawing.Point(25, 89);
            this.uiLabel12.Name = "uiLabel12";
            this.uiLabel12.Size = new System.Drawing.Size(201, 43);
            this.uiLabel12.Style = Sunny.UI.UIStyle.Red;
            this.uiLabel12.TabIndex = 1;
            this.uiLabel12.Text = "学校管理支出比列：";
            this.uiLabel12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel11
            // 
            this.uiLabel11.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel11.Location = new System.Drawing.Point(25, 46);
            this.uiLabel11.Name = "uiLabel11";
            this.uiLabel11.Size = new System.Drawing.Size(201, 43);
            this.uiLabel11.Style = Sunny.UI.UIStyle.Red;
            this.uiLabel11.TabIndex = 0;
            this.uiLabel11.Text = "门卫电费综合支出：";
            this.uiLabel11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 27F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1392, 736);
            this.Controls.Add(this.uiTitlePanel3);
            this.Controls.Add(this.uiTitlePanel2);
            this.Controls.Add(this.uiTitlePanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "form1";
            this.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.Style = Sunny.UI.UIStyle.Red;
            this.Text = "课后延时服务计算器V1.2 power by 刘磊 QQ：247483085";
            this.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.Load += new System.EventHandler(this.Form1_Load);
            this.uiTitlePanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiDataGridView1)).EndInit();
            this.uiTitlePanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiDataGridView2)).EndInit();
            this.uiTitlePanel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Sunny.UI.UITitlePanel uiTitlePanel1;
        private Sunny.UI.UILabel uiLabel1;
        private Sunny.UI.UILabel uiLabel2;
        private Sunny.UI.UITextBox uiTextBox1;
        private Sunny.UI.UIButton JiaRu_button;
        private Sunny.UI.UILabel uiLabel8;
        private Sunny.UI.UILabel jine;
        private Sunny.UI.UILabel banji;
        private Sunny.UI.UILabel uiLabel5;
        private Sunny.UI.UIButton uiButton2;
        private Sunny.UI.UIButton uiButton1;
        private Sunny.UI.UITextBox uiTextBox4;
        private Sunny.UI.UILabel uiLabel4;
        private Sunny.UI.UITextBox uiTextBox3;
        private Sunny.UI.UILabel uiLabel3;
        private Sunny.UI.UIDataGridView uiDataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn num;
        private System.Windows.Forms.DataGridViewTextBoxColumn bj;
        private System.Windows.Forms.DataGridViewTextBoxColumn rs;
        private System.Windows.Forms.DataGridViewTextBoxColumn je;
        private Sunny.UI.UILabel uiLabel6;
        private Sunny.UI.UITitlePanel uiTitlePanel2;
        private Sunny.UI.UIButton uiButton3;
        private Sunny.UI.UILabel uiLabel10;
        private Sunny.UI.UILabel uiLabel9;
        private Sunny.UI.UILabel uiLabel7;
        private Sunny.UI.UITitlePanel uiTitlePanel3;
        private Sunny.UI.UILabel uiLabel16;
        private Sunny.UI.UIComboBox uiComboBox2;
        private Sunny.UI.UILabel uiLabel15;
        private Sunny.UI.UIComboBox uiComboBox1;
        private Sunny.UI.UILabel uiLabel14;
        private Sunny.UI.UITextBox uiTextBox5;
        private Sunny.UI.UILabel uiLabel13;
        private Sunny.UI.UILabel uiLabel12;
        private Sunny.UI.UILabel uiLabel11;
        private Sunny.UI.UIComboBox uiComboBox3;
        private Sunny.UI.UILabel uiLabel18;
        private Sunny.UI.UILabel uiLabel17;
        private Sunny.UI.UILabel uiLabel21;
        private Sunny.UI.UILabel uiLabel20;
        private Sunny.UI.UILabel uiLabel19;
        private Sunny.UI.UILabel uiLabel26;
        private Sunny.UI.UILabel uiLabel27;
        private Sunny.UI.UILabel uiLabel24;
        private Sunny.UI.UILabel uiLabel25;
        private Sunny.UI.UILabel uiLabel22;
        private Sunny.UI.UILabel uiLabel23;
        private Sunny.UI.UILabel uiLabel32;
        private Sunny.UI.UILabel uiLabel33;
        private Sunny.UI.UILabel uiLabel30;
        private Sunny.UI.UILabel uiLabel31;
        private Sunny.UI.UILabel uiLabel28;
        private Sunny.UI.UILabel uiLabel29;
        private Sunny.UI.UIDataGridView uiDataGridView2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private Sunny.UI.UILabel uiLabel34;
        private Sunny.UI.UITextBox uiTextBox6;
        private Sunny.UI.UILabel uiLabel35;
        private Sunny.UI.UILabel uiLabel36;
        private Sunny.UI.UIComboBox uiComboBox5;
        private Sunny.UI.UIComboBox uiComboBox4;
    }
}

